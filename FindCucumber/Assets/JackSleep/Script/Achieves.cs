﻿using UnityEngine;
using UnityEngine.UI;

public class Achieves : MonoBehaviour
{
    [SerializeField] private GameObject gamePause, gameOver, achieves;
    [SerializeField] private GameObject treeBronze, treeSilver, treeGold; //картинки tree
    [SerializeField] private GameObject hollowBronze, hollowSilver, hollowGold; //картинки hollow
    [SerializeField] private GameObject appleBronze, appleSilver, appleGold; //картинки apple
    [SerializeField] private Text treeBronzeScore, treeSilverScore, treeGoldScore; // текстовки tree
    [SerializeField] private Text hollowBronzeScore, hollowSilverScore, hollowGoldScore; // текстовки hollow
    [SerializeField] private Text appleBronzeScore, appleSilverScore, appleGoldScore; // текстовки apple
    [SerializeField] private GameObject AchievesAnimation; //анимация
   
    private Color _colorOn;
    private int _sumTree, _sumHollow, _sumApple, _child;
    private string _achiv;
    
    private void Start()
    {
        //Загружаем сэйв суммы деревьев !!! Включить перед релизом !!!
        _sumTree = PlayerPrefs.GetInt("SumTree");
        _sumHollow = PlayerPrefs.GetInt("SumHollow");
        _sumApple = PlayerPrefs.GetInt("SumApple");
    }

    private void Update()
    {
        if (_sumTree >= 1000 && PlayerPrefs.GetInt("treeBronzeFlag") == 0)
            Animation("treeBronzeFlag", 0);
        if (_sumTree >= 5000 && PlayerPrefs.GetInt("treeSilverFlag") == 0)
            Animation("treeSilverFlag", 1);
        if (_sumTree >= 10000 && PlayerPrefs.GetInt("treeGoldFlag") == 0)
            Animation("treeGoldFlag", 2);

        if (_sumHollow >= 200 && PlayerPrefs.GetInt("hollowBronzeFlag") == 0)
            Animation("hollowBronzeFlag", 3);
        if (_sumHollow >= 500 && PlayerPrefs.GetInt("hollowSilverFlag") == 0)
            Animation("hollowSilverFlag", 4);
        if (_sumHollow >= 1000 && PlayerPrefs.GetInt("hollowGoldFlag") == 0)
            Animation("hollowGoldFlag", 5);

        if (_sumApple >= 50 && PlayerPrefs.GetInt("appleBronzeFlag") == 0)
            Animation("appleBronzeFlag", 6);
        if (_sumApple >= 200 && PlayerPrefs.GetInt("appleSilverFlag") == 0)
            Animation("appleSilverFlag", 7);
        if (_sumApple >= 500 && PlayerPrefs.GetInt("appleGoldFlag") == 0)
            Animation("appleGoldFlag", 8);
    }

  public void Init()
    {
        _colorOn = new Color(1, 1, 1, 1);

        if (_sumTree>=1000)
            treeBronze.GetComponent<Image>().color = _colorOn;
        if (_sumTree >= 5000)
            treeSilver.GetComponent<Image>().color = _colorOn;
        if (_sumTree >= 10000)
            treeGold.GetComponent<Image>().color = _colorOn;

        if (_sumHollow >= 200)
            hollowBronze.GetComponent<Image>().color = _colorOn;
        if (_sumHollow >= 500)
            hollowSilver.GetComponent<Image>().color = _colorOn;
        if (_sumHollow >= 1000)
            hollowGold.GetComponent<Image>().color = _colorOn;

        if (_sumApple >= 50)
            appleBronze.GetComponent<Image>().color = _colorOn;
        if (_sumApple >= 200)
            appleSilver.GetComponent<Image>().color = _colorOn;
        if (_sumApple >= 500)
            appleGold.GetComponent<Image>().color = _colorOn;

        treeBronzeScore.text = "" + Mathf.Clamp(PlayerPrefs.GetInt("SumTree"),0,1000) + "/1000";
        treeSilverScore.text = "" + Mathf.Clamp(PlayerPrefs.GetInt("SumTree"), 0, 5000) + "/5000";
        treeGoldScore.text = "" + Mathf.Clamp(PlayerPrefs.GetInt("SumTree"), 0, 10000) + "/10000";

        hollowBronzeScore.text = "" + Mathf.Clamp(PlayerPrefs.GetInt("SumHollow"), 0, 200) + "/200";
        hollowSilverScore.text = "" + Mathf.Clamp(PlayerPrefs.GetInt("SumHollow"), 0, 500) + "/500";
        hollowGoldScore.text = "" + Mathf.Clamp(PlayerPrefs.GetInt("SumHollow"), 0, 1000) + "/1000";

        appleBronzeScore.text = "" + Mathf.Clamp(PlayerPrefs.GetInt("SumApple"), 0, 50) + "/50";
        appleSilverScore.text = "" + Mathf.Clamp(PlayerPrefs.GetInt("SumApple"), 0, 200) + "/200";
        appleGoldScore.text = "" + Mathf.Clamp(PlayerPrefs.GetInt("SumApple"), 0, 500) + "/500";
    }

    private void Back()
    {
        achieves.transform.gameObject.SetActive(false);
        if (Tree.pauseGame) gamePause.transform.gameObject.SetActive(true);
        else gameOver.transform.gameObject.SetActive(true);
    }

    private void Animation(string _achiv, int _child)
    {
        for (int i = 0; i < 9; i++)
        {
            AchievesAnimation.transform.GetChild(i).gameObject.SetActive(false);
        }
        AchievesAnimation.transform.GetChild(_child).gameObject.SetActive(true);
        AchievesAnimation.GetComponent<Animation>().Play("Achieves");
        PlayerPrefs.SetInt(_achiv, 1);
    }

    public void SumTree()
    {
        _sumTree += 1;
        PlayerPrefs.SetInt("SumTree", _sumTree);
    }

    public void SumHollow()
    {
        _sumHollow += 1;
        PlayerPrefs.SetInt("SumHollow", _sumHollow);
    }

    public void SumApple()
    {
        _sumApple += 1;
        PlayerPrefs.SetInt("SumApple", _sumApple);
    }

}


