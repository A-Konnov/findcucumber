﻿using UnityEngine;

public class Help : MonoBehaviour
{
    [SerializeField] private GameObject helpCanvas, pauseCanvas;

    private void HelpButton()
    {
        pauseCanvas.transform.gameObject.SetActive(false);
        helpCanvas.transform.gameObject.SetActive(true);
    }

    private void BackButton()
    {
        pauseCanvas.transform.gameObject.SetActive(true);
        helpCanvas.transform.gameObject.SetActive(false);
    }
}
