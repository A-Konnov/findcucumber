﻿using UnityEngine;

public class Tap : MonoBehaviour
{
    [SerializeField] private GameObject TapCanvas;
    private bool _tap = true;

	void Start ()
    {
        TapCanvas.transform.gameObject.SetActive(true);
    }
	
	void Update ()
    {
		if (Input.GetMouseButtonDown(0) && _tap)
        {
            TapCanvas.transform.gameObject.SetActive(false);
            _tap = false;
        }
	}
}
