﻿using UnityEngine;

public class Branch : MonoBehaviour
{
    private int typeBranch;

    public void Init()
    {
        //отрисовываем ветку
        for (int i=0; i<9; i++) transform.GetChild(i).gameObject.SetActive(false);
        switch (Tree.vetka)
        {
            case 0:
                typeBranch = 0; //без веток
                transform.GetChild(0).gameObject.SetActive(true);
                break;
            case 1:
                typeBranch = 1; //ветка слева
                transform.GetChild(0).gameObject.SetActive(true);
                transform.GetChild(1).gameObject.SetActive(true);
                break;
            case 2:
                typeBranch = 2; //ветка справа
                transform.GetChild(0).gameObject.SetActive(true);
                transform.GetChild(2).gameObject.SetActive(true);
                break;
            case 3:
                typeBranch = 3; //дупло
                transform.GetChild(3).gameObject.SetActive(true);
                transform.GetChild(4).gameObject.SetActive(true);
                break;
        }
    }

    public int DirectFlag //возвращаем направление ветки
    {
        get { return typeBranch; }
    }
}
