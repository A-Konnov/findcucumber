﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Life : MonoBehaviour
{
    private float speedGame = 1;
    [SerializeField] private Image centerLife, centerSpeed;
    [SerializeField] private Text scoreOut;
    public static float life = 366;
    public static int score = 0;
    public static float speedLife = 2;


    private void OnDestroy()
    {
        life = 360;
        speedLife = 2;
        score = 0;
    }

	private void Update () {
        //пауза в игре
        if (Tree.pauseGame) return;

        if (Tree.playGame == false) return;

        if (life > 0)
        {
            life -= speedLife * speedGame * Time.deltaTime;
            scoreOut.text = "" + score;
            centerLife.rectTransform.sizeDelta = new Vector2(life, 45f);
            centerSpeed.rectTransform.sizeDelta = new Vector2(speedLife*38.8f, 22f);
        }       
        else Tree.playGame = false;
    }
}
