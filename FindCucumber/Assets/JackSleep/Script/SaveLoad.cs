﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SaveLoad : MonoBehaviour {
    [SerializeField] private Text bestScoreOut;

    public static void Save ()
    {
        if (Life.score > PlayerPrefs.GetFloat("BestScore"))
        {
            PlayerPrefs.SetFloat("BestScore", Life.score);
        }      
    }
}
