﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LumberJack : MonoBehaviour {
    private float timer, timerMax=0.15F;
    private bool jackHit = false;
    public static int directLumberJack = 1;

    private void OnDestroy()
    {
        directLumberJack = 1;
    }

    void Start ()
    {
        for (int i = 1; i < 7; i++) transform.GetChild(i).gameObject.SetActive(false);
    }
	
	void Update ()
    {
        //пауза в игре
        if (Tree.pauseGame) return;

        //Джек засыпает
        if (Tree.playGame == false)
        {
            for (int i = 0; i < 4; i++) transform.GetChild(i).gameObject.SetActive(false);
            transform.GetChild(6).gameObject.SetActive(true);
            switch (Tree.direction)
            {
                case 1:
                    transform.GetChild(4).gameObject.SetActive(true);
                    break;
                case -1:
                    transform.GetChild(5).gameObject.SetActive(true);
                    break;
            }
            return;
        }

        if (jackHit)
        {
            //спрайт стоять
            timer += Time.deltaTime;
            if (timer >= timerMax)
            {
                timer = 0;
                jackHit = false;
                for (int i = 0; i < 4; i++) transform.GetChild(i).gameObject.SetActive(false);
                switch (directLumberJack)
                {
                    case 1:
                        transform.GetChild(0).gameObject.SetActive(true);
                        break;
                    case 2:
                        transform.GetChild(2).gameObject.SetActive(true);
                        break;
                }
            }
        }

    }

    public void JackHit()
    {
            if (Input.mousePosition.x < Screen.width / 2)
            {
                directLumberJack = 1;
                for (int i = 0; i < 4; i++) transform.GetChild(i).gameObject.SetActive(false);
                transform.GetChild(1).gameObject.SetActive(true);
            }
            if (Input.mousePosition.x > Screen.width / 2)
            {
                directLumberJack = 2;
                for (int i = 0; i < 4; i++) transform.GetChild(i).gameObject.SetActive(false);
                transform.GetChild(3).gameObject.SetActive(true);
            }
        jackHit = true;
    }
}
