﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tree : MonoBehaviour {
    [SerializeField] private GameObject branch, apple, gameOver, gamePlay, gamePause, achieves;
    [SerializeField] private Text yourScoreOut, bestScoreOut;
    private List<GameObject> tree;
    public static int vetka = 0, direction = 1;
    public static bool playGame = true, pauseGame = false;
    private Apple appleObject;

    [SerializeField] private LumberJack lJack;

    private int vetkaTemp, hollowLeftClick = 0, hollowRightClick = 0, vetkaDouble = 0;
    private float pozition = 500, timer, timerMax = 2F;
    private bool flyFlag = false, moveFlag = false, vetkaFlag = false, tmpFlag = true;
    private Vector2 temp;

    public GameObject spawnPoint;

    private void OnDestroy()
    {
        vetka = 0;
        direction = 1;
        playGame = true;
        pauseGame = false;
    }

    void Start()
    {
        appleObject = null;

        //Создаем дерево
        vetka = 0;
        tree = new List<GameObject>();
        for (int i = 0; i < 6; i++)
        {
            if (i > 3) TestTree();
            GameObject temp = Instantiate(branch);
            var test_branch = temp.GetComponent<Branch>();
            test_branch.Init();
            tree.Add(temp);
            tree[i].transform.position = new Vector2(400, pozition);
            pozition += 200;
        }
    }

    private void DestroyApple()
    {
        appleObject.AppleDestroy();
        Destroy(appleObject.gameObject);
        appleObject = null;
    }

    private void Update()
    {
        //клик по яблоку
        if (appleObject != null && appleObject.tag == "apple")
        {
            if (appleObject.AppleHit())
            {
                Life.speedLife -= 2;
                gameObject.GetComponent<Achieves>().SumApple();
                DestroyApple();
                return;
            }
        }


        // проверяем столкновение с веткой
        GameOver();

        //пауза в игре
        if (pauseGame) return;

        //выводим конец игры
        if (playGame == false)
        {
            timer += Time.deltaTime;
            if (timer >= timerMax)
            {
                timer = 0;
                if (tmpFlag)
                {
                    GameOverScreen();
                    tmpFlag = false;
                }
            }
            return;
        }


        if (flyFlag) FlyTree(direction);
        if (moveFlag) MoveTree();
        else
        {
            var test_branch = tree[0].GetComponent<Branch>();
            if ((test_branch.DirectFlag == 3))
            {
                HollowHit();
            }

            else
            {
                Hit();
            }
        }
    }


    //срубаем дерево
    private void Hit()
    {
        if (Input.GetMouseButtonDown(0) & (Input.mousePosition.y < Screen.height / 1.2F))
        {
            gameObject.GetComponent<Achieves>().SumTree();
            lJack.JackHit();
            flyFlag = true;
            moveFlag = true;
            Life.life = Mathf.Clamp(Life.life + 2, 0, 366);
            Life.speedLife = Mathf.Clamp(Life.speedLife + 0.1f, 0, 10);
            Life.score++;
            if (Input.mousePosition.x < Screen.width / 2) direction = 1;
            if (Input.mousePosition.x > Screen.width / 2) direction = -1;

            if (Random.Range(0, 10) == 5 && appleObject == null && Life.score > 50f && Life.speedLife > 4)
            {
                appleObject = Instantiate(apple).GetComponent<Apple>();
                appleObject.Init(DestroyApple, spawnPoint.transform.position.y);
            }
        }
    }

    //срубание дупла
    private void HollowHit()
    {
        if (Input.GetMouseButtonDown(0) & (Input.mousePosition.y < Screen.height / 1.2F))
        {
            if ((hollowLeftClick >= 2) & (hollowRightClick >= 2))
            {
                gameObject.GetComponent<Achieves>().SumHollow();
                Hit();
                hollowLeftClick = 0;
                hollowRightClick = 0;
            }
            else
            {
                if (Input.mousePosition.x < Screen.width / 2)
                {
                    lJack.JackHit();
                    hollowLeftClick++;
                    switch (hollowLeftClick)
                    {
                        case 1:
                            tree[0].transform.GetChild(3).gameObject.SetActive(false);
                            tree[0].transform.GetChild(5).gameObject.SetActive(true);
                            break;
                        case 2:
                            tree[0].transform.GetChild(5).gameObject.SetActive(false);
                            tree[0].transform.GetChild(7).gameObject.SetActive(true);
                            break;
                    }
                }
                if (Input.mousePosition.x > Screen.width / 2)
                {
                    lJack.JackHit();
                    hollowRightClick++;
                    switch (hollowRightClick)
                    {
                        case 1:
                            tree[0].transform.GetChild(4).gameObject.SetActive(false);
                            tree[0].transform.GetChild(6).gameObject.SetActive(true);
                            break;
                        case 2:
                            tree[0].transform.GetChild(6).gameObject.SetActive(false);
                            tree[0].transform.GetChild(8).gameObject.SetActive(true);
                            break;
                    }
                }
            }
        }
    }

    //тест на тройной повтор и на безысходность
    private void TestTree()
    {
        vetkaFlag = false;
        vetkaTemp = vetka;
        while (vetkaFlag == false)
        {
            if (Life.score > 30) vetka = Random.Range(0, 4);
            else vetka = Random.Range(0, 3);
            switch (vetkaTemp)
            {
                case 1:
                    if (vetka != 2) vetkaFlag = true;
                    break;
                case 2:
                    if (vetka != 1) vetkaFlag = true;
                    break;
                case 3:
                    if (vetka != 3) vetkaFlag = true;
                    break;
                default:
                    vetkaFlag = true;
                    break;
            }

            if (vetkaTemp == vetka || !vetkaFlag) vetkaDouble++;
            else vetkaDouble = 0;
            if (vetkaDouble > 1) vetkaFlag = false;
        }
    }

    //двигаем дерево
    private void MoveTree()
    {
        for (int i = 1; i < 6; i++)
        {
            temp = tree[i].transform.position;
            temp.y -= 1500 * Time.deltaTime;
            tree[i].transform.position = temp;
        }
        if (tree[1].transform.position.y <= 500)
        {
            for (int i = 1; i < 6; i++)
            {
                //правим расположение
                temp = tree[i].transform.position;
                temp.y = Mathf.Ceil(temp.y / 100) * 100;
                tree[i].transform.position = temp;
                moveFlag = false;
            }
        }
    }

    //удаляем дерево
    private void FlyTree(float direction)
    {
        temp = tree[0].transform.position;
        temp.x += 2000 * direction * Time.deltaTime;
        tree[0].transform.position = temp;
        tree[0].transform.Rotate(0, 0, (300 * direction * Time.deltaTime));
        if (tree[1].transform.position.y <= 500)
        {
            Destroy(tree[0]); //уничтожаем [0]
            tree.Remove(tree[0]);

            TestTree();

            GameObject temp = Instantiate(branch);
            tree.Add(temp);
            var test_branch = temp.GetComponent<Branch>();
            test_branch.Init();
            temp.transform.position = new Vector2(400, (tree[4].transform.position.y + 200));
            flyFlag = false;
        }
    }

    //Проверяем на конец игры
    private void GameOver()
    {
        var test_branch = tree[0].GetComponent<Branch>(); //получаем эксземпляр из tree[0]
        if (test_branch.DirectFlag == LumberJack.directLumberJack) playGame = false;  //вызываем свойство экземпляра и сравниваем полученное значение с флагом джека
    }

    private void GameOverScreen()
    {
        gamePlay.transform.gameObject.SetActive(false);
        gameOver.transform.gameObject.SetActive(true);
        yourScoreOut.text = "" + Life.score;
        if (Life.score > PlayerPrefs.GetFloat("BestScore"))
            PlayerPrefs.SetFloat("BestScore", Life.score);
        bestScoreOut.text = "" + PlayerPrefs.GetFloat("BestScore");
    }

    public void Achieves()
    {
        gamePause.transform.gameObject.SetActive(false);
        gameOver.transform.gameObject.SetActive(false);
        gameObject.GetComponent<Achieves>().Init();
        achieves.transform.gameObject.SetActive(true);
    }

    public void Exit()
    {
        Application.Quit();
    }
    
    public void Play()
    {
        gamePause.transform.gameObject.SetActive(false);
        pauseGame = false;
    }

    public void Pause()
    {
        gamePause.transform.gameObject.SetActive(true);
        pauseGame = true;
    }

    public void Restart()
    {
        SceneManager.LoadScene("scena");
    }




}


