﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;
using UnityEngine.UI;

public class CreatObjects : MonoBehaviour
{
    [Header("Setting:")]
    [SerializeField] private int m_numOfCucumbers = 3;
    [SerializeField] private int m_numOfObjects = 10;

    [Header("Prefabs:")]
    [SerializeField] private GameObject m_basePrefab;
    [SerializeField] private Sprite[] m_sprites;

    private int m_currNumOfObjects;
    public int CurrNumOfObjects
    {
        get { return m_currNumOfObjects; }
        set { m_currNumOfObjects = value; }
    }

    private GameObject[] m_allObjects;
    public GameObject[] AllObjects
    {
        get { return m_allObjects; }
        set { m_allObjects = value; }
    }

    private int m_currNumOfCucumbers;
    public int CurrNumOfCucumbers
    {
        get { return m_currNumOfCucumbers; }
        set { m_currNumOfCucumbers = value; }
    }



    void Start()
    {
        m_currNumOfCucumbers = m_numOfCucumbers;
        m_currNumOfObjects = m_numOfObjects;
        BuildCucumberScene();
    }

    void Update()
    {

    }

    public void BuildCucumberScene()
    {
        m_allObjects = new GameObject[m_currNumOfObjects];
        int objIndex = 0;
        m_currNumOfCucumbers = (int)m_currNumOfObjects / 5 + 1;
        Debug.Log(m_currNumOfCucumbers);
        int cucumber = m_currNumOfCucumbers;
        int otherObjects = m_currNumOfObjects - m_currNumOfCucumbers;
        int iteration = 200;

        while (cucumber > 0)
        {
            if (TryCreateObject(m_sprites[0], objIndex))
            {
                cucumber--;
                objIndex++;
            }

        }

        while (otherObjects > 0 && iteration > 0) //Избавляемся от бесконечности
        {
            int rndIndex = Random.Range(1, m_sprites.Length);
            if (TryCreateObject(m_sprites[rndIndex], objIndex))
            {
                otherObjects--;
                objIndex++;
            }
            else
            {
                iteration--;
            }
        }

        ResizeArray();
    }

    private void ResizeArray()
    {
        int nullObj = 0;
        for (int i = 0; i < m_allObjects.Length; i++) //Считаем кол-во null объектов
        {
            if (m_allObjects[i] == null)
            {
                nullObj++;
            }
        }

        if (nullObj != 0) //Если есть null объекты, то изменяем размер массива
        {
            Array.Resize(ref m_allObjects, m_allObjects.Length - nullObj);
        }
    }

    private bool TryCreateObject(Sprite spr, int objIndex)
    {
        float posX = Random.Range(-200, 200) + Random.Range(-10f, 10f);
        float posY = Random.Range(-500, 160) + Random.Range(-10f, 10f);

        Vector3 pos;
        if (spr == m_sprites[0])
        {
            pos = new Vector3(posX, posY, 50);
        }
        else
        {
            pos = new Vector3(posX, posY, 90);
        }

        Quaternion rot = Quaternion.Euler(0, 0, Random.Range(0f, 360f));

        for (int i = 0; i < m_allObjects.Length; i++)
        {
            GameObject currObj = m_allObjects[i];
            if (currObj != null && Vector3.Distance(pos, currObj.transform.position) < 100f)
            {
                return false;
            }
        }

        GameObject instance = Instantiate(m_basePrefab.gameObject, pos, rot);
        instance.GetComponent<SpriteRenderer>().sprite = spr;
        m_allObjects[objIndex] = instance;

        return true;
    }
}
