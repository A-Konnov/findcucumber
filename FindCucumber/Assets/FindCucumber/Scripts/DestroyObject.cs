﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;
using UnityEngine.UI;

public class DestroyObject : MonoBehaviour
{
    [SerializeField] private Sprite m_sprite;
    private bool m_isVictory;
    private int m_destroyedCucumbers = 0;

    private int m_currNumOfObjects;
    private int m_currNumOfCucumbers;
    private float m_reloadLevelTimer;

    void Start()
    {
        m_currNumOfObjects = gameObject.GetComponent<CreatObjects>().CurrNumOfObjects;
        m_currNumOfCucumbers = gameObject.GetComponent<CreatObjects>().CurrNumOfCucumbers;
    }

    private void Update()
    {
        if (!m_isVictory)
        {
            DestroyObjectOnClick();

            if (IsEndOfGame())
            {
                FallObjects();
                m_reloadLevelTimer = Time.time + 1.5f;
            }
        }
        else
        {
            if (Time.time > m_reloadLevelTimer)
            {
                gameObject.GetComponent<CreatObjects>().CurrNumOfObjects++;
                DestroyAllObjects();
                gameObject.GetComponent<CreatObjects>().BuildCucumberScene();
                m_currNumOfObjects = gameObject.GetComponent<CreatObjects>().CurrNumOfObjects;
                m_currNumOfCucumbers = gameObject.GetComponent<CreatObjects>().CurrNumOfCucumbers;
                ResetVictoryState();
                m_destroyedCucumbers = 0;
            }
        }
    }

    private void DestroyObjectOnClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D rcHit = new RaycastHit2D(); //кидаем луч
            rcHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero); //координаты луча

            if (rcHit.collider)
            {
                GameObject currGameObject = rcHit.collider.gameObject;
                if (currGameObject.GetComponent<SpriteRenderer>().sprite == m_sprite)
                {
                    m_destroyedCucumbers++;
                }
                Destroy(currGameObject);
            }

        }
    }

    private bool IsEndOfGame()
    {
        int deletedObjects = 0;
        int numOffCucumbers = 0;
        GameObject[] m_allObjects = gameObject.GetComponent<CreatObjects>().AllObjects;

        for (int i = 0; i < m_allObjects.Length; i++)
        {
            GameObject currObj = m_allObjects[i];
            if (currObj == null)
            {
                deletedObjects++;
            }
            else if (currObj.GetComponent<SpriteRenderer>().sprite == m_sprite)
            {
                numOffCucumbers++;
            }
        }

        if (numOffCucumbers == 0 || deletedObjects > (m_currNumOfCucumbers - 1))
        {
            m_isVictory = CheckVictoryState(numOffCucumbers);
            return true;
        }
        return false;
    }

    private bool CheckVictoryState(int numOffCucumbers)
    {
        if (numOffCucumbers > 0)
        {
            Debug.Log("destroy");
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            return false;
        }
        else
        {
            return true;
        }
    }

    private void DestroyAllObjects()
    {
        GameObject[] m_allObjects = gameObject.GetComponent<CreatObjects>().AllObjects;
        for (int i = 0; i < m_allObjects.Length; i++)
        {
            if (m_allObjects[i] != null)
            {
                Destroy(m_allObjects[i]);
            }
        }
    }

    private void FallObjects()
    {
        GameObject[] m_allObjects = gameObject.GetComponent<CreatObjects>().AllObjects;
        for (int i = 0; i < m_allObjects.Length; i++)
        {
            if (m_allObjects[i] != null)
            {
                Rigidbody2D obj = m_allObjects[i].GetComponent<Rigidbody2D>();
                obj.bodyType = RigidbodyType2D.Dynamic;
            }
        }
    }

    private void ResetVictoryState()
    {
        m_isVictory = false;
    }

}
