﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject2 : MonoBehaviour
{
    [SerializeField] private Sprite m_sprite;
    private GameObject[] m_allObjects;
    private int m_currNumOfCucumbers;
    private int m_destroyedCucumbers;
    private int m_levelNumOfCucumbers;


    private void Start()
    {
        m_allObjects = gameObject.GetComponent<CreatObjects>().AllObjects;
        m_currNumOfCucumbers = gameObject.GetComponent<CreatObjects>().CurrNumOfCucumbers;
        m_levelNumOfCucumbers = m_currNumOfCucumbers;
    }

    private void Update()
    {
        DestroyObjectOnClick();
        Debug.Log(m_currNumOfCucumbers);
        if (m_levelNumOfCucumbers == 0)
        {
            gameObject.GetComponent<CreatObjects>().CurrNumOfObjects++;
            m_currNumOfCucumbers = (int)m_allObjects.Length / 5 + 1;
            DestroyAllObjects();
            gameObject.GetComponent<CreatObjects>().BuildCucumberScene();
            m_levelNumOfCucumbers = m_currNumOfCucumbers;
        }
    }

    private void DestroyObjectOnClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D rcHit = new RaycastHit2D(); //кидаем луч
            rcHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero); //координаты луча

            if (rcHit.collider)
            {
                GameObject currGameObject = rcHit.collider.gameObject;
                if (currGameObject.GetComponent<SpriteRenderer>().sprite == m_sprite)
                {
                    m_levelNumOfCucumbers--;
                }
                Destroy(currGameObject);
            }
        }
    }

    private void DestroyAllObjects()
    {
        GameObject[] m_allObjects = gameObject.GetComponent<CreatObjects>().AllObjects;
        for (int i = 0; i < m_allObjects.Length; i++)
        {
            if (m_allObjects[i] != null)
            {
                Destroy(m_allObjects[i]);
            }
        }
    }
}
